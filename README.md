# Lenovo-Slim-7-Hackintosh (Big Sur )

Model: Lenovo Slim 7 15IIL05 (82AA0029RU)

I was unable to install Samsung MZVLB256HBHQ-000L2, I replaced LITEON CV8-8E128-11 SATA 128GB for tests and additionally installed a second drive (HFM128GDHTNG-8310B) for Windows 10

## Specifications
Intel Core i5 1035G4, Intel Iris Plus Graphics,8 ГБ LPDDR4X 3200 МГц, Wi-Fi 802.11ax, Bluetooth 5.0


##  Screenshot
![Alt text](Screenshot/Screenshot.png?raw=true "Optional Title")

## BIOS Settings (Latest BIOS from Offical Lenovo and [Extended BIOS](https://zhuanlan.zhihu.com/p/184982689)) 

```
Open the extended BIOS.

Advanced -> System Agent (SA) Configuration -> Graphics Configuration -> DVMT Pre-Allocated - 96mb

Advanced -> PCH-IO Configuration -> HD Audio Configuration -> Audio DSP Compliance Mode - UAA (HDA Inbox / IntelISST) 
Advanced -> PCH-IO Configuration -> HD Audio Configuration -> Audio Link Mode - SoundWire
```
## Installation
> magnet:?xt=urn:btih:DA6397A12F616120FA0403FEC116F00C11E07670&tr=http%3A%2F%2Fbt.t-ru.org%2Fann%3Fmagnet&dn=macOS%20Big%20Sur%2011.2%20(20D64)%20legacy%2CUEFI%2CGPT%20%7CClover%20v5.1%20r5129%7C

## Working
1. FN button
2. Intel Iris Plus Graphics
3. Sound (Setting BIOS)
4. TouchPad with Multi-Gesture
5. Power Management & Battery Status
6. Brightness
7. SD Card Reader
8. WiFi + Bluetouch (bluetooth mouse not connecting)
9. Web camera
10. Keyboard backlight

## Not Working
1. Microphone
2. Artifacts at start 10-20 sec
3. NVME Samsung MZVLB256HBHQ-000L2 (replaced LITEON CV8-8E128-11 SATA 128GB)

## Folders
- EFI - Loader on Hard. After installing macOS
- Clover - USB bootloader 

## Caution
> Be careful when configuring the BIOS, especially under System Agent Configuration (SA) -> Graphics Configuration, the screen may not turn on and there is no way to reset the settings. I gave it to a service center, changed the motherboard under warranty - I was lucky. I am not responsible for your technique and your actions, you do everything at your own peril and risk!
